const nextConfig = {
  // reactStrictMode: true,
  // swcMinify: true,
  i18n: {
    locales: ["id", "en"],
    defaultLocale: "id",
    localeDetection: false,
  },
  // Add the following line
  // distDir: "build",
};

module.exports = nextConfig;

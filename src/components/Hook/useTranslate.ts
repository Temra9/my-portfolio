import { useTranslation } from "react-i18next";
import i18nn from "../../../i18n";

const t = (key: string) => {
  return i18nn.t(key);
};

const changeLanguage = (key: string) => {
  const { i18n } = useTranslation();
  i18n.changeLanguage(key);
//   console.log(key, "change langue");
};

export { t, changeLanguage };

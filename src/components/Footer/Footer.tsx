import Layout from "@components/Layout/Layout";
import { motion } from "framer-motion";
import Link from "next/link";
import React from "react";
import { useTranslation } from "react-i18next";
import { BsFacebook, BsGit, BsLinkedin } from "react-icons/bs";

const Footer = () => {

  const { t, i18n } = useTranslation();
  return (
    <footer
      className="w-full border-t-2 border-dark font-medium text-lg dark:text-light
     dark:border-light sm:text-base"
    >
      <Layout className="py-8 flex items-center justify-between lg:flex-col lg:py-6">
        <span>{new Date().getFullYear()}&copy; {t('footer.reserve')}</span>
        <div className="flex items-center lg:py-2">
        {t('footer.build')}
          <span className="text-primary dark:text-primaryDark text-2xl px-2">
            &#9825;
          </span>
          <Link className="underline underline-offset-2" href="/">
            Thierry
          </Link>
        </div>

        <nav className="flex items-center justify-center flex-wrap">
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://www.facebook.com/nambinitsoa.thierry"
            target="_blank"
          >
            <BsFacebook className="font-bold" />
          </motion.a>
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://gitlab.com/Temra9"
            target="_blank"
          >
            <BsGit />
          </motion.a>
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://www.linkedin.com/in/nambinintsoa-thierry-andriamampandrinirina-104866246/"
            target="_blank"
          >
            <BsLinkedin />
          </motion.a>
        </nav>
      </Layout>
    </footer>
  );
};

export default Footer;

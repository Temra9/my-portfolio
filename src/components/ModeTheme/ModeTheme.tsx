import { useTheme } from "next-themes";
import { BsMoonFill, BsSunFill } from "react-icons/bs";
import { useEffect, useState } from "react";
import { motion } from "framer-motion";
import styles from "./styles.module.css";

const ModeTheme = () => {
  const { systemTheme, theme, setTheme } = useTheme();
  const currentTheme = theme === "system" ? systemTheme : theme;
  const [isDarkMode, setIsDarkMode] = useState(currentTheme === "dark");
  useEffect(() => {
    // if (currentTheme === "dark") {
    //   setIsDarkMode(true);
    // }
    console.log(isDarkMode);
  }, []);
  const toggleSwitch = () => {
    setIsDarkMode(!isDarkMode);
    if (currentTheme === "dark") {
      setTheme("light");
    } else {
      setTheme("dark");
    }
  };
  const spring = {
    type: "spring",
    stiffness: 700,
    damping: 100,
  };
  return (
    <div className=" w-full flex items-center justify-center">
      <div
        className={`${styles.switch} ${isDarkMode ? styles.on : ""}`}
        onClick={toggleSwitch}
      >
        <motion.div
          className={`${styles.handle}  ${isDarkMode ? styles.handle_on : ""}`}
          layout
          transition={spring}
        >
          <motion.button initial="initial" animate="animate">
            {isDarkMode ? <BsSunFill /> : <BsMoonFill />}
          </motion.button>
        </motion.div>
      </div>
    </div>
  );
};
export default ModeTheme;

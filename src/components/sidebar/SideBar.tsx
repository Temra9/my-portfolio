import { Variants, motion, useCycle } from "framer-motion";
import MenuToggler from "./MenuToggler";
import Navigation from "./Navigation";

const siderBarVariant: Variants = {
  open: {
    clipPath: `circle(1000px at 40px 40px)`,
    transition: { duration: 0.4 },
  },
  closed: {
    clipPath: `circle(30px at 40px 40px)`,
    transition: { duration: 0.4, delay: 0.4 },
  },
};

const SideBar = () => {
  const [isOpen, setIsOpen] = useCycle(false, true);
  return (
    <nav>
      <motion.div
        className="background"
        variants={siderBarVariant}
        initial={false}
        animate={isOpen ? "open" : "closed"}
      />
      <MenuToggler
        isOpen={isOpen}
        toggle={() => {
          setIsOpen();
        }}
      />
      <Navigation isOpen={isOpen} />
    </nav>
  );
};
export default SideBar;

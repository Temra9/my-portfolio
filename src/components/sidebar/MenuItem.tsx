import { Variants, motion } from "framer-motion";
const menuItemVariant: Variants = {
  open: {
    y: 50,
    opacity: 1,
    transition: {
      duration: 0.4,
    },
  },
  closed: {
    y: 0,
    opacity: 0,
    transition: {
      duration: 0.4,
    },
  },
};

const MenuItem = ({ isOpen }) => {
  return (
    <motion.li
      variants={menuItemVariant}
      initial={false}
      animate={isOpen ? "open" : "closed"}
    >
      <span className="icon-placeholder"></span>
      <span className="text-placeholder"></span>
    </motion.li>
  );
};
export default MenuItem;

import { Variants, motion } from "framer-motion";
import MenuItem from "./MenuItem";
const navigationVariant: Variants = {
  open: {
    transition: {
      staggerChildren: 0.07,
      delayChildren: 0.2,
    },
  },
  closed: {
    transition: {
      staggerChildren: 0.05,
      delayChildren: -1,
    },
  },
};

const Navigation = ({ isOpen }) => {
  return (
    <motion.ul
      variants={navigationVariant}
      initial={false}
      animate={isOpen ? "open" : "closed"}
    >
      {[...Array(5)].map((name, id) => (
        <MenuItem key={id} isOpen={isOpen} />
      ))}
    </motion.ul>
  );
};

export default Navigation;

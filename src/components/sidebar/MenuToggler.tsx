import { TiThMenu } from "react-icons/ti";
import { CgClose } from "react-icons/cg"
import styles from './MenuItem.module.css';
import { Variants, motion } from "framer-motion";
const menuItemVariant: Variants = {
  open: {
    y: 50,
    opacity: 1,
    transition: {
      duration: 0.4,
    },
  },
  closed: {
    y: 0,
    opacity: 0,
    transition: {
      duration: 0.4,
    },
  },
};
const MenuToggler = ({toggle,isOpen}) => {
  return (
    <motion.button  className={styles.menu} onClick={toggle}>
      {isOpen ? <CgClose /> : <TiThMenu />}
    </motion.button>
  );
};
export default MenuToggler;

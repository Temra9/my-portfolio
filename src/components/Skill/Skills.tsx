import { motion } from "framer-motion";
import React from "react";
import { useTranslation } from "react-i18next";
const Skill = ({ name, x, y }) => {
  return (
    <motion.div
      whileHover={{ scale: 1.05 }}
      initial={{ x: 0, y: 0 }}
      whileInView={{ x: x, y: y, transition: { duration: 1.5 } }}
      viewport={{ once: true }}
      className="flex font-semibold bg-dark text-light p-8 shadow-dark py-3 px-6
      items-center justify-center rounded-full absolute cursor-pointer dark:bg-light dark:text-dark
      lg:py-2 lg:px-4 md:text-sm md:py-1.5 md:px-3 xs:bg-transparent xs:dark:bg-transparent
      xs:text-dark xs:dark:text-light xs:font-bold \"
    >
      {name}
    </motion.div>
  );
};
const Skills = () => {
  const { t } = useTranslation();
  return (
    <>
      <h2 className="font-bold text-dark text-8xl w-full mt-64 text-center dark:text-light md:text-6xl xs:text-4xl md:mb-16">
        {t("about.skill")}
      </h2>
      <div
        className="w-full h-screen relative flex 
      items-center justify-center rounded-full bg-circularLight dark:bg-circularDark
      lg:h-[80vh] sm:h-[60vh] xs:h-[50vh] lg:bg-circularDarkLg lg:dark:bg-circularDarkLg
      md:bg-circularLightMd md:dark:bg-circularDarkMd  sm:bg-circularLightSm sm:dark:bg-circularDarkSm
      "
      >
        <motion.div
          whileHover={{ scale: 1.05 }}
          className="flex font-semibold bg-dark text-light p-8 shadow-dark
      items-center justify-center rounded-full dark:bg-light dark:text-dark
      lg:p-6 md:p-4 xs:text-xs xs:p-2
      "
        >
          Web
        </motion.div>
        <Skill name={"CSS"} x={"-25vw"} y={"-10vw"} />
        <Skill name={"HTML"} x={"-20vw"} y={"-1vw"} />
        <Skill name={"Javascript"} x={"20vw"} y={"6vw"} />
        <Skill name={"React"} x={"0vw"} y={"12vw"} />
        <Skill name={"NextJS"} x={"-20vw"} y={"-15vw"} />
        <Skill name={"Angular"} x={"15vw"} y={"-12vw"} />
        <Skill name={"Tailwind CSS"} x={"32vw"} y={"-5vw"} />
        <Skill name={"Php"} x={"0vw"} y={"-20vw"} />
        <Skill name={"Java"} x={"-25vw"} y={"18vw"} />
        <Skill name={"NodeJs"} x={"18vw"} y={"18vw"} />
        <Skill name={"C#"} x={"14vw"} y={"2vw"} />
      </div>
    </>
  );
};

export default Skills;

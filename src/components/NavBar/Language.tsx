import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import iconFrance from "@public/images/icons/icons8-la-france-96.png";
import iconEnglais from "@public/images/icons/icons8-etats-unis-96.png";
import Image from "next/image";

const Language = () => {
  const { i18n } = useTranslation();
  const [selectedLanguage, setSelectedLanguage] = useState("id");
  const [isOpen, setIsOpen] = useState(false);
  useEffect(() => {
    const lang = localStorage.getItem("lang");
    if (lang) setSelectedLanguage(lang);
    const theme = localStorage.getItem("theme");
    if (!theme) localStorage.setItem("theme", "light");
  }, []);
  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language);
    setSelectedLanguage(language);
    localStorage.setItem("lang", language);
    setIsOpen(false);
  };

  const handleDropdownClick = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="relative mx-2">
      <button
        className="flex items-center focus:outline-none"
        onClick={handleDropdownClick}
      >
        {selectedLanguage === "id" ? (
          <Image src={iconFrance} alt="iconFrnace" width={25} />
        ) : (
          <Image src={iconEnglais} alt="iconFrnace" width={25} />
        )}
        <span className="ml-1">
          {" "}
          {selectedLanguage === "id" ? "Français" : "English"}
        </span>
      </button>

      {isOpen && (
        <ul className="absolute w-[110%] mt-2 py-2 bg-white rounded-md shadow-md z-10">
          <li>
            <button
              className="flex items-center w-full px-1 py-2 text-sm text-gray-700 hover:bg-gray-100 focus:outline-none"
              onClick={() => changeLanguage("id")}
            >
              <Image src={iconFrance} alt="iconFrnace" width={25} />
              <span className="ml-1">Français</span>
            </button>
          </li>
          <li>
            <button
              className="flex items-center w-full px-1 py-2 text-sm text-gray-700 hover:bg-gray-100 focus:outline-none"
              onClick={() => changeLanguage("en")}
            >
              <Image src={iconEnglais} alt="iconFrnace" width={25} />
              <span className="ml-1">English</span>
            </button>
          </li>
        </ul>
      )}
    </div>
  );
};

export default Language;

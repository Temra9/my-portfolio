import Link from "next/link";
import Logo from "./Logo";
import { useRouter } from "next/router";
import { BsFacebook, BsGit, BsLinkedin } from "react-icons/bs";
import { motion } from "framer-motion";
import useThemeSwitcher from "@components/Hook/useThemeSwitcher";
import { MoonIcon, SunIcon } from "@components/HereMe/Icon";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Language from "./Language";

const CustomLink = ({ href, title, className = "" }) => {
  const router = useRouter();
  return (
    <Link href={href} className={`${className} relative group`}>
      {title}
      <span
        className={`h-[2px] inline-block bg-dark absolute left-0 -bottom-0.5
        group-hover:w-full transition-[width] ease duration-300 ${
          router.asPath === href ? "w-full" : "w-0"
        } dark:bg-light`}
      >
        &nbsp;
      </span>
    </Link>
  );
};
const CustomMobileLink = ({ href, title, className = "", toggle }) => {
  const router = useRouter();
  const handleClick = () => {
    toggle();
    router.push(href);
  };
  return (
    <button
      className={`${className} relative group text-light dark:text-dark my-2`}
      onClick={handleClick}
    >
      {title}
      <span
        className={`h-[2px] inline-block bg-light absolute left-0 -bottom-0.5
        group-hover:w-full transition-[width] ease duration-300 ${
          router.asPath === href ? "w-full" : "w-0"
        } dark:bg-dark`}
      >
        &nbsp;
      </span>
    </button>
  );
};
const NavBar = () => {
  const { mode, setMode } = useThemeSwitcher();
  const [isOpen, setIsOpen] = useState(false);
  const handleClick = () => {
    setIsOpen(!isOpen);
  };
  const { t, i18n } = useTranslation();

  return (
    <header
      className="w-full px-32 py-8 font-medium flex items-center justify-between
    dark:text-light relative z-10 lg:px-16 md:px-12 sm:px-8"
    >
      <button
        onClick={handleClick}
        className="flex-col justify-center items-center hidden lg:flex"
      >
        <span
          className={`bg-dark dark:bg-light transition-all h-0.5 w-6 rounded-sm block 
        ${isOpen ? "rotate-45 translate-y-1" : "-translate-y-0.5"}`}
        ></span>
        <span
          className={`bg-dark dark:bg-light transition-all h-0.5 w-6 rounded-sm block my-0.5
        ${isOpen ? "opacity-0" : "opacity-100"}`}
        ></span>
        <span
          className={`bg-dark dark:bg-light transition-all h-0.5 w-6 rounded-sm block 
          ${isOpen ? "-rotate-45 -translate-y-1" : "translate-y-0.5"}`}
        ></span>
      </button>
      <div className="w-full flex justify-between items-center lg:hidden ">
        <nav className="capitalize">
          <CustomLink
            href="/"
            title={t("nav.home")}
            className="mr-4 capitalize"
          />
          <CustomLink
            href="/about"
            title={t("nav.about")}
            className="mx-4 capitalize"
          />
          <CustomLink
            href="/projects"
            title={t("nav.projects")}
            className="mx-4"
          />
          {/* <CustomLink
            href="/articles"
            title={t("nav.articles")}
            className="ml-4 bg"
          /> */}
        </nav>
        <nav className="flex items-center justify-center flex-wrap">
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://www.facebook.com/nambinitsoa.thierry"
            target="_blank"
          >
            <BsFacebook className="font-bold" />
          </motion.a>
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://gitlab.com/Temra9"
            target="_blank"
          >
            <BsGit />
          </motion.a>
          <motion.a
            whileHover={{ y: -2, scale: 1.2 }}
            className="w-6 mr-4 text-3xl"
            href="https://www.linkedin.com/in/nambinintsoa-thierry-andriamampandrinirina-104866246/"
            target="_blank"
          >
            <BsLinkedin />
          </motion.a>
          <button
            className={`ml-3 flex items-center justify-center rounded-full p-1
          ${mode === "light" ? "bg-dark text-light" : "bg-light text-dark"}`}
            onClick={() => {
              setMode(mode === "light" ? "dark" : "light");
            }}
          >
            {mode === "dark" ? (
              <SunIcon className={"fill-dark"} />
            ) : (
              // <p>light</p>
              <MoonIcon className={"fill-dark"} />
              // <p>Dark</p>
            )}
          </button>
          <Language />
        </nav>
      </div>

      {isOpen ? (
        <motion.div
          initial={{ scale: 0, opacity: 0, x: "-50%", y: "-50%" }}
          animate={{ scale: 1, opacity: 1 }}
          className="min-w-[70vw] flex flex-col z-30 justify-between items-center fixed top-1/2 left-1/2 -translate-x-1/2
      -translate-y-1/2 bg-dark/90 dark:bg-light/75 rounded-lg backdrop-blur-md py-32 text-light
      dark:text-dark"
        >
          <nav className="flex flex-col justify-center items-center capitalize">
            <CustomMobileLink
              href="/"
              title={t("nav.home")}
              className=""
              toggle={handleClick}
            />
            <CustomMobileLink
              href="/about"
              title={t("nav.about")}
              className=""
              toggle={handleClick}
            />
            <CustomMobileLink
              href="/projects"
              title={t("nav.projects")}
              className=""
              toggle={handleClick}
            />
            {/* <CustomMobileLink
              href="/articles"
              title={t("nav.articles")}
              className=""
              toggle={handleClick}
            /> */}
          </nav>
          <nav className="flex items-center justify-center flex-wrap mt-2">
            <motion.a
              whileHover={{ y: -2, scale: 1.2 }}
              className="w-6 mr-3 text-2xl sm:mx-1"
              href="/"
              target="_blank"
            >
              <BsFacebook className="font-bold" />
            </motion.a>
            <motion.a
              whileHover={{ y: -2, scale: 1.2 }}
              className="w-6 mr-3 text-2xl sm:mx-1"
              href="/"
              target="_blank"
            >
              <BsGit />
            </motion.a>
            <motion.a
              whileHover={{ y: -2, scale: 1.2 }}
              className="w-6 mr-3 text-2xl sm:mx-1"
              href="/"
              target="_blank"
            >
              <BsLinkedin />
            </motion.a>
            <button
              className={`ml-3 flex items-center justify-center rounded-full p-1
          ${mode === "light" ? "bg-dark text-light" : "bg-light text-dark"}`}
              onClick={() => {
                setMode(mode === "light" ? "dark" : "light");
              }}
            >
              {mode === "dark" ? (
                <SunIcon className={"fill-dark"} />
              ) : (
                // <p>light</p>
                <MoonIcon className={"fill-dark"} />
                // <p>Dark</p>
              )}
            </button>
            <Language />
          </nav>
        </motion.div>
      ) : null}
      <div className="absolute left-[48%] top-2 translate-x-[50%] md:left-[40%] sm:left-[35%]">
        <Logo />
      </div>
    </header>
  );
};
export default NavBar;

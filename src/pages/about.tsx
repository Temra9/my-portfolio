import Layout from "@components/Layout/Layout";
import AnimationText from "@components/Text/AnimationText";
import Head from "next/head";
import React, { useEffect, useRef } from "react";
import profilPic from "../../public/images/profile/developer-pic-3.jpg";
import Image from "next/image";
import { useInView, useMotionValue, useSpring } from "framer-motion";
import Skills from "@components/Skill/Skills";
import Experience from "@components/Experience/Experience";
import Education from "@components/Experience/Education";
import TransitionEffet from "@components/Transition/TransitionEffet";
import { useTranslation } from "react-i18next";

const AnimateNumber = ({ value }) => {
  const ref = useRef(null);
  const motionValue = useMotionValue(0);
  const springValue = useSpring(motionValue, { duration: 3000 });
  const isInView = useInView(ref, { once: true });
  useEffect(() => {
    if (isInView) {
      motionValue.set(value);
    }
  }, [isInView, value, motionValue]);
  useEffect(() => {
    springValue.on("change", (latest) => {
      if (ref.current && latest.toFixed(0) <= value) {
        ref.current.textContent = latest.toFixed(0);
      }
    });
  }, [springValue, value]);
  return <span ref={ref}></span>;
};
const About = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>PortFolio | About Page</title>
        <meta name="description" content="This is my profil" />
      </Head>
      <TransitionEffet />
      <main className="flex flex-col justify-center items-center w-full dark:text-light">
        <Layout className="pt-16">
          <AnimationText
            text={t("about.title")}
            className="mb-16 lg:!text-7xl sm:!text-6xl
          xs:!text-4xl sm:mb-8"
          />
          <div className="grid w-full grid-cols-8 gap-16 sm:gap-8">
            <div
              className="col-span-3 flex flex-col items-start justify-start xl:col-span-4 md:order-2
            md:col-span-8"
            >
              <h2 className="mb-4 text-lg font-bold uppercase text-dark/75 dark:text-light">
                {t("about.bio")}
              </h2>
              <p className="font-medium mb-2">{t("about.text1")}</p>

              <p className="font-medium mb-2">{t("about.text2")}</p>

              <p className="font-medium mb-2">{t("about.text3")}</p>
            </div>
            <div
              className="col-span-3 relative h-max rounded-2xl border-2 border-solid border-dark
            bg-light p-8 dark:bg-dark dark:border-light xl:col-span-4 md:order-1 md:col-span-8"
            >
              <div
                className="absolute top-0 -right-3 -z-10 w-[102%] h-[103%] rounded-2xl bg-dark
              dark:bg-light"
              />
              <Image
                src={profilPic}
                alt={"protfolio by Thierry"}
                className="w-full h-auto rounded-2xl"
                priority
                sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
              />
            </div>
            <div
              className="col-span-2 flex flex-col items-end justify-between xl:col-span-8 xl:flex-row
            xl:items-center md:order-3"
            >
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:!text-6xl sm:!text-5xl xs:!text-4xl">
                  <AnimateNumber value={10} />+
                </span>
                <h2
                  className="text-xl font-medium capitalize text-dark/75 dark:text-light
                xl:text-center sm:text-base md:text-lg xs:text-sm"
                >
                  {t("about.client")}
                </h2>
              </div>
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:!text-6xl sm:!text-5xl xs:!text-4xl">
                  <AnimateNumber value={4} />+
                </span>
                <h2
                  className="text-xl font-medium capitalize text-dark/75 dark:text-light
                 xl:text-center sm:text-base md:text-lg xs:text-sm"
                >
                  {t("about.projet")}
                </h2>
              </div>
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:!text-6xl sm:!text-5xl xs:!text-4xl">
                  <AnimateNumber value={1} />
                </span>
                <h2
                  className="text-xl font-medium capitalize text-dark/75 dark:text-light
                 xl:text-center sm:text-base md:text-lg xs:text-sm"
                >
                  {t("about.exp")}
                </h2>
              </div>
            </div>
          </div>
          <Skills />
          <Experience />
          <Education />
        </Layout>
      </main>
    </>
  );
};

export default About;

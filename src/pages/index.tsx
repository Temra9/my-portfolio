import Layout from "@components/Layout/Layout";
import ModeTheme from "@components/ModeTheme/ModeTheme";
import Head from "next/head";
import Image from "next/image";
import profilPic from "../../public/images/profile/developer-pic-4.png";
import lightBulb from "../../public/images/svgs/miscellaneous_icons_1.svg";
import AnimationText from "@components/Text/AnimationText";
import Link from "next/link";
import { FiExternalLink } from "react-icons/fi";
import HereMe from "@components/HereMe/HereMe";
import TransitionEffet from "@components/Transition/TransitionEffet";
import { useTranslation } from "react-i18next";
export default function Home() {
  const { t } = useTranslation();
  return (
    <>
      {/* <ModeTheme /> */}
      <Head>
        <title>PortFolio | by Thierry</title>
        <meta name="description" content="This is my profil" />
      </Head>
      <TransitionEffet />
      <main className="flex items-center text-dark w-full min-h-screen dark:text-light">
        <Layout className="pt-0 md:p-16 sm:pt-8">
          <div className="flex items-center justify-between w-full lg:flex-col">
            <div className="w-1/2 md:w-full">
              {/* <Image
                src={profilPic}
                alt="protfolio"
                className="w-full h-auto lg:hidden md:inline-block md:w-full"
                priority
                sizes="(max-width: 768px) 100vw,(max-width: 1200px) 2vw,2vw"
              /> */}
              <Image
                src={profilPic}
                alt="portfolio"
                className="w-full h-auto lg:hidden md:inline-block md:w-full"
                priority
                width={1920}
                height={1080}
              />
            </div>
            <div className="w-1/2 flex flex-col items-center sef-center lg:w-full lg:text-center">
              <AnimationText
                text={t("home.title")}
                className="!text-6xl !text-left xl:!text-5xl lg:!text-center lg:!text-6xl md:!text-5xl
                sm:!text-3xl"
              />
              <p className="my-4 text-base font-medium">
                {t("home.description")}
              </p>
              <div className="flex items-center self-start mt-2 lg:self-center">
                <Link
                  href="/cv.pdf"
                  target="_blank"
                  className="flex items-center bg-dark text-light p-2.5 px-6 rounded-lg text-lg 
                  font-semibold hover:bg-light hover:text-dark border-2 border-solid
                  border-transparent hover:border-dark dark:bg-light dark:text-dark
                  hover:dark:bg-dark hover:dark:text-light hover:dark:border-light
                  md:p-2 md:px-4 md:text-base"
                  download={true}
                >
                  Resume <FiExternalLink className="w-6 ml-1" />
                </Link>
                <Link
                  href="mailto:nandriamampandrinirina@gmail.com"
                  className="ml-4 font-medium capitalize text-dark underline dark:text-light"
                >
                  Contact
                </Link>
              </div>
            </div>
          </div>
        </Layout>
        <HereMe />
        <div className="absolute right-8 bottom-8 inline-block w-24 md:hidden">
          <Image
            src={lightBulb}
            alt="portfolio by Thierry"
            className="w-full h-auto"
          />
        </div>
      </main>
    </>
  );
}

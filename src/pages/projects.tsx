import Layout from "@components/Layout/Layout";
import AnimationText from "@components/Text/AnimationText";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import React from "react";
import { BsGit } from "react-icons/bs";
import image1 from "../../public/images/projects/crypto-screener-cover-image.jpg";
import { motion } from "framer-motion";
import TransitionEffet from "@components/Transition/TransitionEffet";
import { useTranslation } from "react-i18next";
const FrameImage = motion(Image);
const FeatureProjet = ({ type, title, summury, img, link, github }) => {
  return (
    <article
      className="w-full flex items-center justify-between rounded-3xl rounded-br-2xl border
    border-solid border-dark bg-light shadow-2xl p-12 relative dark:bg-dark dark:border-light
    lg:flex-col lg:p-8 xs:rounded-2xl xs:rounded-br-3xl xs:p-4"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] rounded-br-3xl h-[103%] rounded-[2.5rem] bg-dark
      dark:bg-light xs:-right-2 sm:h-[102%] xs:w-full xs:rounded-[1.5rem]"
      />
      <Link
        className="w-1/2 cursor-pointer overflow-hidden rounded-lg lg:w-full"
        href={link}
        target="_blank"
      >
        <FrameImage
          whileHover={{ scale: 1.05 }}
          transition={{ duration: 0.2 }}
          src={img}
          alt={title}
          width={1300}
          height={1000}
          className="w-full h-auto"
        />
      </Link>
      <div className="w-1/2 flex flex-col items-start justify-between pl-6 lg:pl-0 lg:pt-6 lg:w-full">
        <span className="text-primary dark:text-primaryDark font-medium text-xl xs:text-base">
          {type}
        </span>
        {/* <Link href={link} target="_blank" className=" hover:underline"> */}
        <h2 className="w-full my-2 text-left text-4xl font-bold dark:text-light sm:text-sm">
          {title}
        </h2>
        {/* </Link> */}
        <p className="my-2 text-dark dark:text-light font-medium sm:text-sm">
          {summury}
        </p>
        <div className="mt-2 flex items-center">
          {/* <Link href={github} target="_blank" className="w-10">
            <BsGit className="w-10 mr-3 text-5xl" />
          </Link>
          <Link
            href={link}
            target="_blank"
            className="ml-4 rounded-lg bg-dark text-light p-2 p-x-6 text-lg
            font-semibold dark:bg-light dark:text-dark sm:px-4 sm:text-base"
          >
            Visite Projects
          </Link> */}
        </div>
      </div>
    </article>
  );
};
const Projet = ({ type, title, img, link, github }) => {
  return (
    <article
      className="w-full flex flex-col items-center justify-center rounded-2xl border
    border-solid border-dark bg-light p-6 relative dark:bg-dark dark:border-light
    xs:p-4"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] rounded-br-3xl h-[103%] rounded-[2rem] bg-dark
      dark:bg-light md:-right-2 md:w-[101%] md:h-[102%] xs:rounded[1.5rem]"
      />
      <Link
        className="w-full cursor-pointer overflow-hidden rounded-lg"
        href={link}
        target="_blank"
      >
        <FrameImage
          whileHover={{ scale: 1.05 }}
          transition={{ duration: 0.2 }}
          src={img}
          alt={title}
          width={1900}
          height={1300}
          className="w-full h-auto"
          priority
          sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
        />
      </Link>
      <div className="w-full flex flex-col items-start justify-between mt-4">
        <span
          className="text-primary dark:text-primaryDark font-medium text-xl
        lg:text-lg md:text-base"
        >
          {type}
        </span>
        {/* <Link href={link} target="_blank" className=" hover:underline"> */}
        <h2 className="w-full my-2 text-left text-3xl font-bold lg:text-2xl">
          {title}
        </h2>
        {/* </Link> */}
        <div className="w-full mt-2 flex items-center justify-between">
          {/* <Link
            href={link}
            target="_blank"
            className="rounded-lg text-lg font-semibold underline md:text-base"
          >
            Visite
          </Link>
          <Link href={github} target="_blank" className="w-8 md:w-6">
            <BsGit className="w-10 mr-3 text-5xl" />
          </Link> */}
        </div>
      </div>
    </article>
  );
};
const Projects = () => {
  const { t } = useTranslation();
  const features: any[] = t("project.features", { returnObjects: true });
  const projets: any[] = t("project.data", { returnObjects: true });
  return (
    <>
      <Head>
        <title>Portfolio by Thierry | Projects</title>
        <meta name="description" content="This is my profil" />
      </Head>
      <TransitionEffet />
      <main className="w-full mb-16 flex flex-col items-center justify-center dark:text-light">
        <Layout className="pt-16">
          <AnimationText
            text={t("project.title")}
            className="mb-16 lg:!text-7xl sm:mb-8 sm:!text-6xl xs:!text-4xl"
          />
          <div className="grid grid-cols-12 gap-24 gap-y-32 xl:gap-x-16 lg:gap-x-8 md:gap-y-24 sm:gap-x-0">
            <div className="col-span-12">
              <FeatureProjet
                title={features[0].title}
                summury={features[0].summury}
                link={"/"}
                type={features[0].type}
                img={features[0].image}
                github={"/"}
              />
            </div>
            <div className="col-span-6 sm:col-span-12">
              <Projet
                title={projets[0].title}
                link={projets[0].link}
                type={projets[0].type}
                img={projets[0].image}
                github={"/"}
              />
            </div>
            <div className="col-span-6 sm:col-span-12">
              <Projet
                title={projets[1].title}
                link={projets[1].link}
                type={projets[1].type}
                img={projets[1].image}
                github={"/"}
              />
            </div>
            <div className="col-span-12">
              <FeatureProjet
                title={features[1].title}
                summury={features[1].summury}
                link={"/"}
                type={features[1].type}
                img={features[1].image}
                github={"/"}
              />
            </div>
            {/* <div className="col-span-6 sm:col-span-12">
            <Projet
                title={projets[2].title}
                link={projets[2].link}
                type={projets[2].type}
                img={projets[2].image}
                github={"/"}
              />
            </div>
            <div className="col-span-6 sm:col-span-12">
            <Projet
                title={projets[3].title}
                link={projets[3].link}
                type={projets[3].type}
                img={projets[3].image}
                github={"/"}
              />
            </div> */}
          </div>
        </Layout>
      </main>
    </>
  );
};

export default Projects;

import Layout from "@components/Layout/Layout";
import AnimationText from "@components/Text/AnimationText";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import React, { useRef } from "react";
import picture1 from "../../public/images/articles/create loading screen in react js.jpg";
import picture2 from "../../public/images/articles/form validation in reactjs using custom react hook.png";
import { motion, useMotionValue } from "framer-motion";
import TransitionEffet from "@components/Transition/TransitionEffet";

const FrameImage = motion(Image);
const MovingImg = ({ img, title, link }) => {
  const x = useMotionValue(0);
  const y = useMotionValue(0);
  const ref = useRef(null);
  const handleMouse = (event) => {
    ref.current.style.display = "inline-block";
    x.set(event.page);
    y.set(-10);
  };
  const hadleMousLeave = (event) => {
    ref.current.style.display = "none";
    x.set(0);
    y.set(0);
  };
  return (
    <Link
      href={link}
      target="_blank"
      onMouseMove={handleMouse}
      onMouseLeave={hadleMousLeave}
    >
      <h2 className="capitalize font-semibold text-xl hover:underline">
        {title}
      </h2>
      <FrameImage
        initial={{ opacity: 0 }}
        whileInView={{ opacity: 1, transition: { duration: 0.2 } }}
        ref={ref}
        style={{ x: x, y: y }}
        src={img}
        alt={title}
        className="z-10 w-96 h-auto hidden absolute rounded-lg md:!hidden"
        priority
        sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
      />
    </Link>
  );
};
const Article = ({ img, title, date, link }) => {
  return (
    <motion.li
      initial={{ y: 200 }}
      whileInView={{ y: 0, transition: { duration: 0.5, ease: "easeInOut" } }}
      viewport={{ once: true }}
      className=" relative w-full py-4 p-4 my-4 rounded-xl flex items-center justify-between
    bg-light text-dark first:mt-0 border border-solid border-dark border-r-4 border-b-4
    dark:border-light dark:bg-dark dark:text-light sm:flex-col"
    >
      <MovingImg img={img} title={title} link={link} />
      <span className="text-primary dark:text-primaryDark font-semibold pl-4 sm:self-start
      sm:pl-0 xs:text-sm">
        {date}
      </span>
    </motion.li>
  );
};
const FeatureArticle = ({ time, title, summury, img, link }) => {
  return (
    <li
      className="col-span-1 w-full p-4 bg-light border
    border-solid border-dark rounded-2xl relative dark:bg-dark dark:border-light"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] rounded-br-3xl h-[103%] rounded-[2rem] bg-dark
      dark:bg-light"
      />
      <Link
        className="w-full cursor-pointer inline-block overflow-hidden rounded-lg"
        href={link}
        target="_blank"
      >
        <FrameImage
          whileHover={{ scale: 1.05 }}
          transition={{ duration: 0.2 }}
          src={img}
          alt={title}
          className="w-full h-auto"
        />
      </Link>
      <Link href={link} target="_blank">
        <h2 className="capitalize font-bold my-2 text-2xl hover:underline xs:text-lg">
          {title}
        </h2>
      </Link>
      <p className="text-sm mb-2">{summury}</p>
      <span className=" text-primary dark:text-primaryDark font-semibold">
        {time}
      </span>
    </li>
  );
};
const Articles = () => {
  return (
    <>
      <Head>
        <title>PortFolio | Articles Page</title>
        <meta name="description" content="This is my profil" />
      </Head>
      <TransitionEffet/>
      <main className="flex flex-col justify-center items-center w-full mb-16 overflow-hidden dark:text-light">
        <Layout className="mt-16">
          <AnimationText
            text={"Words Can Change The World!"}
            className="mb-16 lg:!text-7xl sm:mb-8 sm:!text-6xl xs:!text-4xl"
          />
          <ul className="grid grid-cols-2 gap-16 md:grid-cols-1 md:gap-y-16">
            <FeatureArticle
              title={
                "Build A Custom Pagination Component In Reactjs From Scratch"
              }
              summury={`Learn how to build a custom pagination component in ReactJS from scratch.Follow this step-by-step guide to integrate Pagination component in your ReactJS project.`}
              time={"9 min read"}
              link={"/"}
              img={picture1}
            />
            <FeatureArticle
              title={
                "Build A Custom Pagination Component In Reactjs From Scratch"
              }
              summury={`Learn how to build a custom pagination component in ReactJS from scratch.Follow this step-by-step guide to integrate Pagination component in your ReactJS project.`}
              time={"9 min read"}
              link={"/"}
              img={picture1}
            />
          </ul>
          <h2 className="font-bold w-full text-center text-4xl my-16 mt-32">
            All Articles
          </h2>
          <ul>
            {[...Array(5)].map((_, index) => {
              return (
                <Article
                  key={index}
                  img={picture2}
                  title={
                    "Form Validation In Reactjs: Build A Reusable Custom Hook For Inputs And Error Handling"
                  }
                  date={"22 Mars 2023"}
                  link={"/"}
                />
              );
            })}
          </ul>
        </Layout>
      </main>
    </>
  );
};

export default Articles;

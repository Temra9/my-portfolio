import Footer from "@components/Footer/Footer";
import NavBar from "@components/NavBar/NavBar";
import "@styles/globals.css";
import { AnimatePresence } from "framer-motion";
// import { Montserrat } from "next/font/google";
import { useRouter } from "next/router";
import i18n from "../../i18n";
import { useEffect } from "react";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  // const router = useRouter()
  // if (router.locale) {
  //   i18n.changeLanguage(router.locale);
  // }
  useEffect(() => {
    const lang = localStorage.getItem("lang");
    if (lang) {
      i18n.changeLanguage(lang);
    }
  }, []);
  return (
    <main className={`font-mont bg-light dark:bg-dark w-full min-h-screen`}>
      <NavBar />
      <AnimatePresence mode="wait">
        <Component key={router.asPath} {...pageProps} />
      </AnimatePresence>
      <Footer />
    </main>
  );
}

export default MyApp;
